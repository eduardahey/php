<?php
class ContaCorrente{
    private $numeroConta;
    private $nomeCorrentista;
    private $saldo;
    function __construct($numeroConta,$nomeCorrentista){
        $this->numeroConta = $numeroConta;
        $this->nomeCorrentista = $nomeCorrentista;
        $this->saldo = 0;
    }
    function alterarNome($nome){
        $this->nomeCorrentista = $nome;
    }
    function deposito($saldo2){
        $this->saldo = $saldo2;
    }
    function saque($saldo3){
        $this->saldo = $this->saldo-$saldo3;
    }
    function mostrarSaldo(){
        return $this->saldo;
    }
}
    $p = new ContaCorrente(194,"Duda");
    echo($p->mostrarSaldo());
?>
