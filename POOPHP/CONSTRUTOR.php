<?php
class Retangulo{
    private $base;
    private $altura;
 
    function __construct($base, $altura){
    $this->base = $base;
    $this->altura = $altura;
    }
    function setlados($base, $altura){
        $this->base = $base;
        $this->altura = $altura;
    }
    function getbase(){
        return $this->base;
    }
    function getaltura(){
        return $this->altura;
    }
    function area(){
        return $this->base*$this->altura;
    }
    function perimetro(){
       return (2*$this->base)+(2*$this->altura);
    }
}
    $lado1 = $_POST['l1'];
    $lado2 = $_POST['l2'];
    $rp = 2;
    $local = new Retangulo($lado1,$lado2);
    $piso = new Retangulo(4,6);
    echo("A quantidade de pisos necessários para o local é:" . $local->area()/$piso->area()."</br>");
    echo("A quantidade de rodapés necessários para o local é:" . $local->perimetro()/$rp);
?>