<?php
class Pessoa{
    private $nome;
    private $idade;
    private $peso;
    private $altura;
 
    function __construct($nome,$idade,$peso, $altura){
        $this->nome = $nome;
        $this->idade = $idade;
        $this->peso = $peso;
        $this->altura = $altura;
    }
    function setenvelhecer($novaIdade){
        if($novaIdade<21){
            $this->altura = (($novaIdade-$this->idade)*0.5)+$this->altura;
        }
        $this->idade = $novaIdade;
    }
    function getenvelhecer(){
        return $this->idade;
    }
    function setengordar($peso2){
        $this->peso = $peso2;
    }
    function getengordar(){
       return $this->peso;
    }
    function setemagrecer($peso3){
        $this->peso = $peso3;
    }
    function getemagrecer(){
       return $this->peso;
    }
    function getcrescer(){
        return $this->altura;
    }
}
    $p  = new Pessoa("Duda",16,66.5,163);
    echo($p->getcrescer());
?>